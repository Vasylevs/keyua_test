<h1>Test for KeyUA</h1>
<h3>Employee service</h3>
<p>console commands</p>

<code>
    php bin/console company:employee employeeType <br>
    php bin/console employee:can employeeType workType
</code>

<hr>
<blockquote>
<p>**company:employee** - show employee work can do</p>
<p>**employee:can** - check if employee can do work</p>
</blockquote>

