<?php

namespace App\Command;

use App\MessageFormatter\CompanyEmployeeFormatter;
use App\Service\CompanyEmployee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CompanyEmployeeCommand extends Command
{
    private const EMPLOYEE_TYPE = 'employeeType';

    protected static $defaultName = 'company:employee';

    /**
     * @var CompanyEmployee
     */
    private $companyEmployee;
    /**
     * @var CompanyEmployeeFormatter
     */
    private $companyEmployeeFormatter;

    /**
     * CompanyEmployeeCommand constructor.
     * @param CompanyEmployee $companyEmployee
     * @param CompanyEmployeeFormatter $companyEmployeeFormatter
     */
    public function __construct(
        CompanyEmployee $companyEmployee,
        CompanyEmployeeFormatter $companyEmployeeFormatter
    ) {
        $this->companyEmployee = $companyEmployee;
        $this->companyEmployeeFormatter = $companyEmployeeFormatter;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Show employee type work can do')
            ->addArgument(
                self::EMPLOYEE_TYPE,
                InputArgument::REQUIRED,
                'Input type employee'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $employeeType = $input->getArgument(self::EMPLOYEE_TYPE);

        $employee = $this->companyEmployee->getEmployee($employeeType);
        if (null === $employee) {
            $io->error($this->companyEmployeeFormatter->getUnknownEmployeeTypeMessage());
            $io->note($this->companyEmployeeFormatter->getAllEmployeeTypeMessage());
            return 0;
        }

        $io->success($this->companyEmployeeFormatter->getEmployeeWorksMessage($employee));

        return 0;
    }
}
