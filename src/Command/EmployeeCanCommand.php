<?php

namespace App\Command;

use App\MessageFormatter\CompanyEmployeeFormatter;
use App\Service\CompanyEmployee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EmployeeCanCommand extends Command
{
    private const EMPLOYEE_TYPE = 'employeeType';
    private const WORK_TYPE = 'workType';

    protected static $defaultName = 'employee:can';
    /**
     * @var CompanyEmployee
     */
    private $companyEmployee;
    /**
     * @var CompanyEmployeeFormatter
     */
    private $companyEmployeeFormatter;

    /**
     * EmployeeCanCommand constructor.
     * @param CompanyEmployee $companyEmployee
     * @param CompanyEmployeeFormatter $companyEmployeeFormatter
     */
    public function __construct(
        CompanyEmployee $companyEmployee,
        CompanyEmployeeFormatter $companyEmployeeFormatter
    ) {
        $this->companyEmployee = $companyEmployee;
        $this->companyEmployeeFormatter = $companyEmployeeFormatter;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Check employee can do some work type')
            ->addArgument(
                self::EMPLOYEE_TYPE,
                InputArgument::REQUIRED,
                'Input type employee'
            )
            ->addArgument(
                self::WORK_TYPE,
                InputArgument::REQUIRED,
                'Input type work'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $employeeType = $input->getArgument(self::EMPLOYEE_TYPE);
        $workType = $input->getArgument(self::WORK_TYPE);

        dump([
            $employeeType,
            $workType
        ]);


        $employee = $this->companyEmployee->getEmployee($employeeType);
        if (null === $employee) {
            $io->error($this->companyEmployeeFormatter->getUnknownEmployeeTypeMessage());
            $io->note($this->companyEmployeeFormatter->getAllEmployeeWorkTypeMessage());
            return 0;
        }


        $io->success($this->companyEmployeeFormatter->getEmployeeCanDoWorkMessage(
            $employee->getEmployeeType(),
            $workType,
            $this->companyEmployee->isEmployeeDoWork($employee, $workType)
        ));

        return 0;
    }
}
