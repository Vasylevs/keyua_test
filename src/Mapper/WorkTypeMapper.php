<?php

namespace App\Mapper;

class WorkTypeMapper
{
    public const DRAW = 'draw';
    public const WRITE_CODE = 'write code';
    public const TEST_CODE = 'test code';
    public const SET_TASK = 'set tasks';
    public const COMMUNICATION_WITH_MANAGER = 'communication with manager';

    public const WORK_TYPE_MAPPER = [
        self::COMMUNICATION_WITH_MANAGER,
        self::WRITE_CODE,
        self::TEST_CODE,
        self::SET_TASK,
        self::DRAW
    ];
}
