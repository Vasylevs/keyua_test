<?php

namespace App\Mapper;

class EmployeeTypeMapper
{
    public const PROGRAMMER = 'programmer';
    public const DESIGNER = 'designer';
    public const TESTER = 'tester';
    public const MANAGER = 'manager';

    public const EMPLOYEE_TYPE_MAPPER = [
        self::MANAGER,
        self::DESIGNER,
        self::TESTER,
        self::PROGRAMMER
    ];
}
