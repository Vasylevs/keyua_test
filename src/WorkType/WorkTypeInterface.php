<?php

namespace App\WorkType;

interface WorkTypeInterface
{
    /**
     * @return string
     */
    public function getWorkName(): string;
}
