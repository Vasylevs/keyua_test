<?php

namespace App\WorkType;

use App\Mapper\WorkTypeMapper;

class SetTasks extends AbstractWorkType implements WorkTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $wortTypeName = WorkTypeMapper::SET_TASK;
}
