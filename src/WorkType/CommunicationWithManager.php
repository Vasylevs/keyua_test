<?php

namespace App\WorkType;

use App\Mapper\WorkTypeMapper;

class CommunicationWithManager extends AbstractWorkType implements WorkTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $wortTypeName = WorkTypeMapper::COMMUNICATION_WITH_MANAGER;
}
