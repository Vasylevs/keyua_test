<?php

namespace App\WorkType;

use App\Mapper\WorkTypeMapper;

class Draw extends AbstractWorkType implements WorkTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $wortTypeName = WorkTypeMapper::DRAW;
}
