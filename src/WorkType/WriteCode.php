<?php

namespace App\WorkType;

use App\Mapper\WorkTypeMapper;

class WriteCode extends AbstractWorkType implements WorkTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $wortTypeName = WorkTypeMapper::WRITE_CODE;
}
