<?php

namespace App\WorkType;

use App\Mapper\WorkTypeMapper;

class TestCode extends AbstractWorkType implements WorkTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $wortTypeName = WorkTypeMapper::TEST_CODE;
}
