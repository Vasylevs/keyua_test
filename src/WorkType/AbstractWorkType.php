<?php

namespace App\WorkType;

abstract class AbstractWorkType
{
    /**
     * @var string
     */
    protected $wortTypeName;

    /**
     * @return string
     */
    public function getWorkName(): string
    {
        return $this->wortTypeName;
    }
}
