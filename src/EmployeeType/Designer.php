<?php

namespace App\EmployeeType;

use App\Mapper\EmployeeTypeMapper;

class Designer extends AbstractEmployeeType implements EmployeeTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $employeeType = EmployeeTypeMapper::DESIGNER;
}
