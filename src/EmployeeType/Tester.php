<?php

namespace App\EmployeeType;

use App\Mapper\EmployeeTypeMapper;

class Tester extends AbstractEmployeeType implements EmployeeTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $employeeType = EmployeeTypeMapper::TESTER;
}
