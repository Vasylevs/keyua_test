<?php

namespace App\EmployeeType;

use App\Mapper\EmployeeTypeMapper;

class Programmer extends AbstractEmployeeType implements EmployeeTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $employeeType = EmployeeTypeMapper::PROGRAMMER;
}
