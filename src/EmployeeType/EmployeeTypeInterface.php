<?php

namespace App\EmployeeType;

use App\WorkType\WorkTypeInterface;

interface EmployeeTypeInterface
{
    /**
     * EmployeeTypeInterface constructor.
     * @param WorkTypeInterface[] $workType
     */
    public function __construct(array $workType);

    /**
     * @return string
     */
    public function getEmployeeType(): string;

    /**
     * @return WorkTypeInterface[]
     */
    public function getWorkType(): array;
}
