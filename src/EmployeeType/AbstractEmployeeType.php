<?php

namespace App\EmployeeType;

use App\WorkType\WorkTypeInterface;

abstract class AbstractEmployeeType
{
    /**
     * @var string
     */
    protected $employeeType;
    /**
     * @var WorkTypeInterface[]
     */
    private $workType;

    /**
     * AbstractEmployeeType constructor.
     * @param WorkTypeInterface[] $workType
     */
    public function __construct(array $workType)
    {
        $this->workType = $workType;
    }

    /**
     * @return string
     */
    public function getEmployeeType(): string
    {
        return $this->employeeType;
    }

    /**
     * @return WorkTypeInterface[]
     */
    public function getWorkType(): array
    {
        return $this->workType;
    }
}
