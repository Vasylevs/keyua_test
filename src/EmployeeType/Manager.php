<?php

namespace App\EmployeeType;

use App\Mapper\EmployeeTypeMapper;

class Manager extends AbstractEmployeeType implements EmployeeTypeInterface
{
    /**
     * @inheritdoc
     */
    protected $employeeType = EmployeeTypeMapper::MANAGER;
}
