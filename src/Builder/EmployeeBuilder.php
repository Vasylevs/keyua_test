<?php

namespace App\Builder;

use App\EmployeeType\Designer;
use App\EmployeeType\EmployeeTypeInterface;
use App\EmployeeType\Manager;
use App\EmployeeType\Programmer;
use App\EmployeeType\Tester;
use App\WorkType\CommunicationWithManager;
use App\WorkType\Draw;
use App\WorkType\SetTasks;
use App\WorkType\TestCode;
use App\WorkType\WriteCode;

class EmployeeBuilder
{
    /**
     * @var EmployeeTypeInterface[]
     */
    private $employees;

    /**
     * EmployeeEmployee constructor.
     */
    public function __construct()
    {
        $this->buildProgrammer();
        $this->buildTester();
        $this->buildDesigner();
        $this->buildManager();
    }

    /**
     * @return EmployeeTypeInterface[]
     */
    public function getAllEmployees(): array
    {
        return $this->employees;
    }

    /**
     * @return void
     */
    private function buildProgrammer(): void
    {
        $this->employees[] = new Programmer([
            new WriteCode(),
            new TestCode(),
            new CommunicationWithManager()
        ]);
    }

    /**
     * @return void
     */
    private function buildTester(): void
    {
        $this->employees[] = new Tester([
            new TestCode(),
            new CommunicationWithManager(),
            new SetTasks()
        ]);
    }

    /**
     * @return void
     */
    private function buildDesigner(): void
    {
        $this->employees[] = new Designer([
            new Draw(),
            new CommunicationWithManager()
        ]);
    }

    /**
     * @return void
     */
    private function buildManager(): void
    {
        $this->employees[] = new Manager([
            new SetTasks()
        ]);
    }
}
