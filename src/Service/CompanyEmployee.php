<?php

namespace App\Service;

use App\Builder\EmployeeBuilder;
use App\EmployeeType\EmployeeTypeInterface;

class CompanyEmployee
{
    /**
     * @var EmployeeTypeInterface[]
     */
    private $employees;

    /**
     * CompanyEmployee constructor.
     * @param EmployeeBuilder $employeeBuilder
     */
    public function __construct(EmployeeBuilder $employeeBuilder)
    {
        $this->employees = $employeeBuilder->getAllEmployees();
    }

    /**
     * @param string $employeeType
     * @return EmployeeTypeInterface|null
     */
    public function getEmployee(string $employeeType): ?EmployeeTypeInterface
    {
        foreach ($this->employees as $employee) {
            if ($employee->getEmployeeType() === strtolower($employeeType)) {
                return $employee;
            }
        }

        return null;
    }

    /**
     * @param EmployeeTypeInterface $employee
     * @param string $workName
     * @return bool
     */
    public function isEmployeeDoWork(EmployeeTypeInterface $employee, string $workName): bool
    {
        foreach ($employee->getWorkType() as $workType) {
            if ($workType->getWorkName() === strtolower($workName)) {
                return true;
            }
        }

        return false;
    }
}
