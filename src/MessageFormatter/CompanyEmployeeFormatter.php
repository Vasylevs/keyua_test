<?php

namespace App\MessageFormatter;

use App\EmployeeType\EmployeeTypeInterface;
use App\Mapper\EmployeeTypeMapper;
use App\Mapper\WorkTypeMapper;

class CompanyEmployeeFormatter
{
    private const WORK_TYPE_MESSAGE_TPL = '- %s' . PHP_EOL;
    private const ALL_EMPLOYEE_TYPE_TPL = 'Choose next employee type: %s';
    private const ALL_WORK_TYPE_TPL = 'Choose next work type: %s';
    private const UNKNOWN_EMPLOYEE_TYPE_TPL = 'Unknown employee type';
    private const CAN_DO_WORK_RESPONSE_TPL = '%s can %s: %s';

    /**
     * @param EmployeeTypeInterface $employee
     * @return string
     */
    public function getEmployeeWorksMessage(EmployeeTypeInterface $employee): string
    {
        $employeeWorksStr = '';

        foreach ($employee->getWorkType() as $workType) {
            $employeeWorksStr .= sprintf(self::WORK_TYPE_MESSAGE_TPL, $workType->getWorkName());
        }

        return $employeeWorksStr;
    }

    /**
     * @return string
     */
    public function getAllEmployeeTypeMessage(): string
    {
        return sprintf(self::ALL_EMPLOYEE_TYPE_TPL, implode(', ', EmployeeTypeMapper::EMPLOYEE_TYPE_MAPPER));
    }

    /**
     * @return string
     */
    public function getUnknownEmployeeTypeMessage(): string
    {
        return self::UNKNOWN_EMPLOYEE_TYPE_TPL;
    }

    /**
     * @return string
     */
    public function getAllEmployeeWorkTypeMessage(): string
    {
        return $this->getAllEmployeeTypeMessage() . PHP_EOL
            . sprintf(self::ALL_WORK_TYPE_TPL, implode(', ', WorkTypeMapper::WORK_TYPE_MAPPER));
    }

    /**
     * @param string $employeeType
     * @param string $workType
     * @param bool $canDo
     * @return string
     */
    public function getEmployeeCanDoWorkMessage(string $employeeType, string $workType, bool $canDo): string
    {
        return sprintf(
            self::CAN_DO_WORK_RESPONSE_TPL,
            $employeeType,
            $workType,
            $canDo ? 'true' : 'false'
        );
    }
}
